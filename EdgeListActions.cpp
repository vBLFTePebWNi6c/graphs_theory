#include "EdgeListActions.h"

void EdgeListActions::readGraph( const string &fileName, edge_list_t &edgList, int &nodeCount, bool &isWeighted, bool &isDirected )
{
    int edgeCount;
    string line;
    ifstream input ( fileName );
    if ( input.is_open() and getline( input, line ) )
    {
        vector<string> tokens;
        boost::split(tokens, line, boost::is_any_of(" "));
        if ( tokens.size() == 3 )
        {
            nodeCount = stoi( tokens[1] );
            edgeCount = stoi( tokens[2] );
        }
        else 
        {
            cout << "Incorrect input format. Expected 2 values in frist line instead of this: " << line << endl;
            exit(-1);
        }
        
        getline( input, line );
        boost::split(tokens, line, boost::is_any_of(" "));
        if ( tokens.size() == 2 )
        {
            isDirected = stoi( tokens[0] );
            isWeighted = stoi( tokens[1] );
        }
        else 
        {
            cout << "Incorrect input format. Expected 2 values in second line instead of this: " << line << endl;
            exit(-1);
        }
        
        while ( getline( input, line ) )
        {
            boost::split(tokens, line, boost::is_any_of(" "));
            
            if( isWeighted ) 
                edgList.insert( tuple<int, int, int>( stoi( tokens[0] ), stoi( tokens[1] ), stoi( tokens[2] ) ) );
            else
                edgList.insert( tuple<int, int, int>( stoi( tokens[0] ), stoi( tokens[1] ), 0 ) );
                
            
        }
    }
    else cout << "Unable to open file";
}

void EdgeListActions::addEdge( int from, int to, int weight, edge_list_t &edgList )
{
    edgList.insert( tuple<int, int, int>( from, to, weight ) );
}

void EdgeListActions::removeEdge( int from, int to, edge_list_t &edgList )
{
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        if ( get<0>( *it ) == from && get<1>( *it ) == to )
        {
            edgList.erase( it );
            return;
        }
}

int EdgeListActions::changeEdge( int from, int to, int newWeight, edge_list_t &edgList )
{
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        if ( get<0>( *it ) == from && get<1>( *it ) == to )
        {
            int oldWeight = get<2>( *it );
            edgList.erase( it );
            edgList.insert( tuple<int, int, int>( from, to, newWeight ) );
            return oldWeight;
        }
        
    return -1;
}

void EdgeListActions::transformToAdjMatrix( edge_list_t &edgList, int nodeCount, adjacency_matrix_t &adjMat )
{
    adjMat.clear();
    for( size_t i = 0; i < nodeCount; i++ )
        adjMat.push_back( adj_mat_line_t( nodeCount, 0 ) );
     
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        adjMat[ get<0>( *it ) - 1 ][ get<1>( *it ) - 1 ] = get<2>( *it );
}

void EdgeListActions::transformToAdjList( edge_list_t &edgList, int nodeCount, adjacency_list_t &adjList)
{
    adjList.clear();
    for( size_t i = 0; i < nodeCount; i++ )
        adjList.push_back( adj_list_line_t() );
        
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        adjList[ get<0>( *it ) ].insert( tuple<int, int, int>( get<0>( *it ), get<1>( *it ), get<2>( *it ) ) ) ;
}

void EdgeListActions::writeGraph( string fileName, edge_list_t &edgList, int nodeCount, int isWeighted, int isDirected )
{
    string line;
    ofstream output ( fileName );
    
    output << (char)EDGE_LIST << " " << nodeCount << " " << edgList.size() << endl << isWeighted << " " << isDirected << endl;
    
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
    {
        output << to_string( get<0>( *it ) ) << " " <<  to_string( get<1>( *it ) ) << " " << to_string( get<2>( *it ) ) << endl;
    }
}

void EdgeListActions::printGraph( edge_list_t &edgList, int nodeCount, int isWeighted, int isDirected )
{
    cout << "Graph type: Edge list" << endl 
         << "Node count: " << nodeCount << endl
         << "Edge count: " << edgList.size() << endl
         << "Is it weighted: " << (isWeighted ? "TRUE" : "FALSE") << endl
         << "Is it directed: " << (isDirected ? "TRUE" : "FALSE") << endl;
    
    string line;
    for ( auto it = edgList.begin(); it != edgList.end(); it++ )
    {
        cout << "Edge (" << to_string( get<0>( *it ) ) << ", " <<  to_string( get<1>( *it ) ) << ") with weight " << to_string( get<2>( *it ) ) << endl;
    }
}
