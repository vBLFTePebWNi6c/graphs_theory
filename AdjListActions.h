#pragma once
#include <iostream>
#include <fstream>
#include "custom_types.h"

using namespace std;
using namespace GraphTypes;

class AdjListActions
{
public:
    static void readGraph( const string &fileName, adjacency_list_t &adjList, int &nodeCount, bool &isWeighted, bool &isDirected )
    {
        adjList.clear();
        string line;
        ifstream input ( fileName );
        if ( input.is_open() and getline( input, line ) )
        {
            vector<string> tokens;
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() == 2 )
                nodeCount = stoi( tokens[1] );
            else 
            {
                cout << "Incorrect input format. Expected 2 values in frist line instead of this: " << line << endl;
                exit(-1);
            }
            
            getline( input, line );
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() == 2 )
            {
                isDirected = stoi( tokens[0] );
                isWeighted = stoi( tokens[1] );
            }
            else 
            {
                cout << "Incorrect input format. Expected 2 values in second line instead of this: " << line << endl;
                exit(-1);
            }
            
            int node_num = 0;
            while ( getline( input, line ) )
            {
                node_num++;
                GraphTypes::split(tokens, line, ' ');
                adj_list_line_t adj_list_line;
                
                if( isWeighted )
                {
                    for ( size_t i = 0; i < tokens.size(); i += 2 )
                        adj_list_line.insert( tuple<int, int, int>( node_num, stoi( tokens[i] ), stoi( tokens[i + 1] ) ) );
                }
                else
                {
                    for ( size_t i = 0; i < tokens.size(); i++ )
                        adj_list_line.insert( tuple<int, int, int>( node_num, stoi( tokens[i] ), 0 ) );
                }
                    
                adjList.push_back( adj_list_line );
            }
        }
        else cout << "Unable to open file";
    }
    
    static void addEdge( int from, int to, int weight, adjacency_list_t &adjList )
    {
        adjList[from - 1].insert( tuple<int, int, int>( from, to, weight ) );
    }
    
    static void removeEdge( int from, int to, adjacency_list_t &adjList )
    {
        for( auto it = adjList[from - 1].begin(); it != adjList[from - 1].end(); it++ )
            if ( get<0>( *it ) == from && get<1>( *it ) == to )
            {
                adjList[from - 1].erase( it );
                return;
            }
    }
    
    static int changeEdge( int from, int to, int newWeight, adjacency_list_t &adjList )
    {
        for( auto it = adjList[from - 1].begin(); it != adjList[from - 1].end(); it++ )
            if ( get<0>( *it ) == from && get<1>( *it ) == to )
            {
                int oldWeight = get<2>( *it );
                adjList[from - 1].erase( it );
                adjList[from - 1].insert( tuple<int, int, int>( from, to, newWeight ) );
                return oldWeight;
            }
            
        return -1;
    }
    
    static void transformToAdjMatrix( adjacency_list_t &adjList, adjacency_matrix_t &adjMat )
    {
        adjMat.clear();
        for( size_t i = 0; i < adjList.size(); i++ )
        {
            adjMat.push_back( adj_mat_line_t( adjList.size(), 0 ) );
            for ( auto it = adjList[i].begin(); it != adjList[i].end(); it++ )
                adjMat[ get<0>( *it ) - 1 ][ get<1>( *it ) - 1 ] = get<2>( *it );
        }
    }
    
    static void transformToListOfEdges( adjacency_list_t &adjList, edge_list_t &edgList )
    {
        edgList.clear();
        for( size_t i = 0; i < adjList.size(); i++ )
        {
            for ( auto it = adjList[i].begin(); it != adjList[i].end(); it++ )
                edgList.push_back( *it );
        }
    }
    
    
    static void writeGraph( string fileName, adjacency_list_t &adjList, int nodeCount, int isWeighted, int isDirected )
    {    
        string line;
        ofstream output ( fileName );
        
        output << (char)ADJ_LIST << " " << nodeCount << endl << isDirected << " " << isWeighted << endl;
        
        int node_num = 1;
        for ( size_t i = 0; i < adjList.size(); i++ )
        {
            while ( get<0>( *adjList[i].begin() ) > node_num )
            {
                output << endl;
                node_num++;
            }
            
            line = "";
            for ( auto it = adjList[i].begin(); it != adjList[i].end(); it++ )
            {
                if ( isWeighted == 1 )
                    line += to_string( get<1>( *it ) + 1 ) + " " + to_string( get<2>( *it ) ) + " ";
                else
                    line += to_string( get<1>( *it ) + 1 ) + " ";
            }
            
            if ( line[ line.size() - 1 ] == ' ' ) line.assign( line.begin(), line.end() - 1 );
            
            output << line << endl;
            node_num++;
        }
        
        while ( nodeCount > node_num )
        {
            output << endl;
            node_num++;
        }
    }
    
    
    static void printGraph( const adjacency_list_t &adjList, const int nodeCount, const int isWeighted, const int isDirected )
    {
        cout << "Graph type: Adjacency list" << endl 
            << "Node count: " << nodeCount << endl
            << "Is it weighted: " << (isWeighted ? "TRUE" : "FALSE") << endl
            << "Is it directed: " << (isDirected ? "TRUE" : "FALSE") << endl;
        
        string line;
        for ( size_t i = 0; i < adjList.size(); i++ )
        {
            line = "Node " + to_string(i + 1) + ( adjList[i].begin() == adjList[i].end() ? " adjacent to nothing." : " adjacent to:\n" ) ;
            for ( auto it = adjList[i].begin(); it != adjList[i].end(); it++ )
            {
                line += "-- node " + to_string( get<1>( *it ) ) + " with weight " + to_string( get<2>( *it ) ) + "\n";
            }
            cout << line << endl;
        }
    }
};

