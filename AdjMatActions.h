#pragma once
#include <iostream>
#include <fstream>
#include "custom_types.h"

using namespace std;
using namespace GraphTypes;

class AdjMatActions
{
public:
    static void readGraph( const string &fileName, adjacency_matrix_t &adjMat, int &nodeCount, bool &isWeighted, bool &isDirected )
    {
        adjMat.clear();
        string line;
        ifstream input ( fileName );
        if ( input.is_open() and getline( input, line ) )
        {
            vector<string> tokens;
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() == 4 )
            {
                //cout << '(' << tokens[1] << ')' << '(' << tokens[2] << ')' << '(' << tokens[3] << ')' << endl;
                nodeCount = stoi( tokens[1] );
                //cout << nodeCount << endl;
                isWeighted = stoi( tokens[2] );
                //cout << isWeighted << endl;
                isDirected = stoi( tokens[3] );
                //cout << isDirected << endl;
            }
            else 
            {
                cout << "Incorrect input format. Expected 4 values in frist line instead of this: " << line << endl;
                exit(-1);
            }
            
            while ( getline( input, line ) )
            {
                GraphTypes::split(tokens, line, ' ');
                adj_mat_line_t adj_mat_line;
                for ( size_t i = 0; i < tokens.size(); i++ )
                    adj_mat_line.push_back( stoi( tokens[i] ) );
                    
                adjMat.push_back( adj_mat_line );
            }
        }
        else cout << "Unable to open file"; 
    }    
    
    static void addEdge( int from, int to, int weight, adjacency_matrix_t &adjMat )
    {
        adjMat[from - 1][to - 1] = weight;
    }
     
    static void removeEdge( int from, int to, adjacency_matrix_t &adjMat )
    {
        adjMat[from - 1][to - 1] = 0;
    }
      
    static int changeEdge( int from, int to, int newWeight, adjacency_matrix_t &adjMat )
    {
        adjMat[from - 1][to - 1] = newWeight;
    } 
    
    static void transformToAdjList( adjacency_matrix_t &adjMat, adjacency_list_t &adjList)
    {
        adjList.clear();
        for ( size_t i = 0; i < adjMat.size(); i++ )
        {
            adj_list_line_t adj_list_line;
            for ( size_t j = 0; j < adjMat.size(); j++ )
                if ( adjMat[i][j] != 0 ) 
                    adj_list_line.insert( tuple<int, int, int>( i + 1, j + 1, adjMat[i][j] ) );
            adjList.push_back( adj_list_line );
        }
    }
    
    static void transformToListOfEdges( adjacency_matrix_t &adjMat, edge_list_t &edgList)
    {
        edgList.clear();
        for ( size_t i = 0; i < adjMat.size(); i++ )
        {
            for ( size_t j = 0; j < adjMat.size(); j++ )
                if ( adjMat[i][j] != 0 ) 
                {
                    edgList.push_back( tuple<int, int, int>( i + 1, j + 1, adjMat[i][j] ) );
                }
        }
    }
    
    static void writeGraph( string fileName, adjacency_matrix_t &adjMat, int nodeCount, int isWeighted, int isDirected )
    {
        string line;
        ofstream output ( fileName );
        
        output << (char)ADJ_MAT << " " << nodeCount << " " << isWeighted << " " << isDirected << endl;
        
        for ( size_t i = 0; i < adjMat.size(); i++ )
        {
            line = "";
            for ( size_t j = 0; j < adjMat.size(); j++ )
            {
                line += to_string( adjMat[i][j] ) + " ";
            }
            
            if ( line[ line.size() - 1 ] == ' ' ) line.assign( line.begin(), line.end() - 1 );
            
            output << line << endl;
        }
    }
    
    static void printGraph( const adjacency_matrix_t &adjMat, const int nodeCount, const int isWeighted, const int isDirected )
    {
        cout << "Graph type: Adjacency matrix" << endl 
            << "Node count: " << nodeCount << endl
            << "Is it weighted: " << (isWeighted ? "TRUE" : "FALSE") << endl
            << "Is it directed: " << (isDirected ? "TRUE" : "FALSE") << endl;
        
        string line;
        for ( size_t i = 0; i < adjMat.size(); i++ )
        {
            line = "";
            for ( size_t j = 0; j < adjMat.size(); j++ )
            {
                line += to_string( adjMat[i][j] ) + " ";
            }
            cout << line << endl;
        }
    }
};
