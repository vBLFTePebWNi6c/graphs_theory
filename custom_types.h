#pragma once
#include <vector>
#include <string>
#include <tuple>
#include <set>

using namespace std;

namespace GraphTypes
{

typedef std::vector< int > adj_mat_line_t;
typedef std::vector< adj_mat_line_t > adjacency_matrix_t;

typedef std::set< std::tuple < int, int, int > > adj_list_line_t;
typedef std::vector < adj_list_line_t > adjacency_list_t;

typedef std::tuple < int, int, int > edge_list_line_t;
typedef std::vector < edge_list_line_t > edge_list_t;

enum graph_representation_t
{
    ADJ_MAT = 'C',
    ADJ_LIST = 'L',
    EDGE_LIST = 'E',
    UNKNOWN = 'U'
};

void split( vector< string > &tokens, const string &line, const char separator )
{
    tokens.clear();
    string token = "";
    for( auto it = line.begin(); it != line.end(); it++ )
    {
        if( (char)*it != separator )
            token += *it;
        else
        {
            tokens.push_back( token );
            token = "";
        }
    }
    
    if ( token.size() != 0 )
        tokens.push_back( token );
}

}
