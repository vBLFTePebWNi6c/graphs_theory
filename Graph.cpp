#include <Graph.h>

Graph::Graph() : _graph_type( UNKNOWN ), _isWeighted( false ), _isDirected( false ), _nodeCount( 0 ) {}

void Graph::readGraph( string fileName )
{
    string line;
    ifstream input ( fileName );
    if ( input.is_open() and getline( input, line ) )
    {
        vector<string> tokens;
        boost::split(tokens, line, boost::is_any_of(" "));
        if ( tokens.size() > 0 )
            switch ( tokens[0].c_str()[0] )
            {
                case ADJ_MAT : 
                {
                    input.close();
                    AdjMatActions::readGraph( fileName, _adjacency_matrix, _nodeCount, _isWeighted, _isDirected );
                    _graph_type = ADJ_MAT; 
                    break;
                }
                
                case ADJ_LIST : 
                {
                    input.close();
                    AdjListActions::readGraph( fileName, _adjacency_list, _nodeCount, _isWeighted, _isDirected ); 
                    _graph_type = ADJ_LIST;
                    break;
                }
                
                case EDGE_LIST : 
                {
                    input.close();
                    EdgeListActions::readGraph( fileName, _edge_list, _nodeCount, _isWeighted, _isDirected ); 
                    _graph_type = EDGE_LIST;
                    break;
                }
                
                default : cout << "Wrong representation type: " << line[0] << endl; break;
            }
        else
            cout << "Wrong representation type: " << line << endl;
    }
    else 
    {
        cout << "Unable to open file." << endl;
    } 
}

void Graph::addEdge( int from, int to, int weight )
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::addEdge( from, to, weight, _adjacency_matrix );
            break;
        }
        
        case ADJ_LIST : 
        {
            AdjListActions::addEdge( from, to, weight, _adjacency_list ); 
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::addEdge( from, to, weight, _edge_list ); 
            break;
        }
        
        default : cout << "Unable add edge to graph of UNKNOWN type." << endl; break; 
    }
}
    
void Graph::removeEdge( int from, int to )
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::removeEdge( from, to, _adjacency_matrix );
            break;
        }
        
        case ADJ_LIST : 
        {
            AdjListActions::removeEdge( from, to, _adjacency_list ); 
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::removeEdge( from, to, _edge_list ); 
            break;
        }
        
        default : cout << "Unable remove edge from graph of UNKNOWN type." << endl; break; 
    }
}

int Graph::changeEdge( int from, int to, int newWeight )
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::changeEdge( from, to, newWeight, _adjacency_matrix );
            break;
        }
        
        case ADJ_LIST : 
        {
            AdjListActions::changeEdge( from, to, newWeight, _adjacency_list ); 
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::changeEdge( from, to, newWeight, _edge_list ); 
            break;
        }
        
        default : cout << "Unable add edge to graph of UNKNOWN type." << endl; break; 
    }
}

void Graph::transformToAdjList()
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::transformToAdjList( _adjacency_matrix, _adjacency_list );
            _graph_type = ADJ_LIST;
            break;
        }
        
        case ADJ_LIST : return;
        
        case EDGE_LIST : 
        {
            EdgeListActions::transformToAdjList( _edge_list, _nodeCount, _adjacency_list );
            _graph_type = ADJ_LIST;
            break;
        }
        
        default : cout << "Can not transform UNKNOWN graph type." << endl; break;
    }
}

void Graph::transformToAdjMatrix()
{
    switch ( _graph_type )
    {
        case ADJ_MAT : return;
        
        case ADJ_LIST : 
        {
            AdjListActions::transformToAdjMatrix( _adjacency_list, _adjacency_matrix );
            _graph_type = ADJ_MAT;
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::transformToAdjMatrix( _edge_list, _nodeCount, _adjacency_matrix );
            _graph_type = ADJ_MAT;
            break;
        }
        
        default : cout << "Can not transform UNKNOWN graph type." << endl; break;
    }
}

void Graph::transformToListOfEdges()
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::transformToListOfEdges( _adjacency_matrix, _edge_list );
            _graph_type = EDGE_LIST;
            break;
        }
        
        case ADJ_LIST :         
        {
            AdjListActions::transformToListOfEdges( _adjacency_list, _edge_list );
            _graph_type = EDGE_LIST;
            break;
        }
        
        case EDGE_LIST : return;
        
        default : cout << "Can not transform UNKNOWN graph type." << endl; break;
    }
}

void Graph::writeGraph( string fileName )
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::writeGraph( fileName, _adjacency_matrix, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 );
            break;
        }
        
        case ADJ_LIST : 
        {
            AdjListActions::writeGraph( fileName, _adjacency_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::writeGraph( fileName, _edge_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
            break;
        }
        
        default : cout << "Can not write graph of UNKNOWN type." << endl; break;
    }
}

void Graph::printGraph()
{
    switch ( _graph_type )
    {
        case ADJ_MAT : 
        {
            AdjMatActions::printGraph( _adjacency_matrix, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 );
            break;
        }
        
        case ADJ_LIST : 
        {
            AdjListActions::printGraph( _adjacency_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
            break;
        }
        
        case EDGE_LIST : 
        {
            EdgeListActions::printGraph( _edge_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
            break;
        }
        
        default : cout << "Can not print graph of UNKNOWN type." << endl; break;
    }
}
