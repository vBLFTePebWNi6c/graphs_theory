#include <Graph.h>

int main( int argc, char** argv )
{
    Graph g( "./fully_connected_graph.txt" );
    //g.writeGraph( "./test1" );
    //g.transformToAdjList();
    //g.writeGraph( "./test2" );
    //g.transformToListOfEdges();
    //g.writeGraph( "./test3" );
    Graph spanningTree;
    cout << "Prima's algorithm: " << endl;
    spanningTree = g.getSpanningTreePrima();
    spanningTree.printGraph();
    
    //cout << "Kruscal's algorithm: " << endl;
    //spanningTree = g.getSpanningTreeKruscal();
    //spanningTree.printGraph();
    
    ////cout << "Boruvka's algorithm: " << endl;
    ////spanningTree = g.getSpanningTreeBoruvka();
    ////spanningTree.printGraph();
    //
    //g.readGraph( "./euler_graph.txt" );
    ////cout << "Fleri's algorithm: " << endl;
    ////vector< int > path = g.getEuleranTourFleri();
    ////for( auto it = path.begin(); it != path.end(); it++ )
    ////    cout << "->" << *it ;
    ////cout << endl;
    ////
    ////cout << "Effective algorithm: " << endl;
    ////path = g.getEuleranTourEffective();
    ////for( auto it = path.begin(); it != path.end(); it++ )
    ////    cout << "->" << *it ;
    ////cout << endl;
    //
    //g.readGraph( "./bipart_graph.txt" );
    //vector< pair< int, int > > mmb = g.getMaximumMatchingBipart();
    //cout << "Maximum matching bipart:" << endl;
    //for( auto it = mmb.begin(); it != mmb.end(); it++ )
    //    cout << it->first << " " << it->second << endl;
    
    return 0;
}
