#include <vector>
#include <iostream>

using namespace std;

class DSU
{    
public:
    DSU( int N )
    {
        p = vector< int >( N, 0 );
        rank = vector< int >( N, 0 );
        for( int i = 1; i <= N; i++ )
            MakeSet( i );
    }

    void MakeSet(int x) 
    {
        p[x] = x;
    }
    
    int find(int x) 
    {
        return ( x == p[x] ? x : p[x] = find(p[x]) );
    }
    
    bool unite(int x, int y) 
    {
        if ( (x = find(x)) == (y = find(y)) )
            return false;
        
        if ( rank[x] <  rank[y] )
            p[x] = y;
        else
            p[y] = x;
    
        if ( rank[x] == rank[y] )
            ++rank[x];
            
        return true;
    }
    
private:
    vector< int > p, rank;
};

class DSU_plus
{    
public:
    DSU_plus( int N )
    {
        components = vector< vector< int > >( N, vector< int >() );
        for( int i = 0; i < N; i++ )
            components[i].push_back(i);
    }
    
    int find(int x) 
    {
        for( int i = 0; i < components.size(); i++ )
            for( int j = 0; j < components[i].size(); j++ )
                if ( components[i][j] == x )
                    return i;
    }
    
    bool unite(int x, int y) 
    {
        int x_c = find(x);
        int y_c = find(y);
        //cout << x + 1 << " in " << x_c + 1 << endl << y + 1 << " in " << y_c + 1 << endl;
        if ( x_c == y_c )
            return false;
        else
        {
            for( int i = 0; i < components[y_c].size(); i++ )
            {
                components[x_c].push_back( components[y_c][i] );
            }
            components.erase( components.begin() + y_c );
            return true;
        }
    }
    
    vector< vector< int > > components;
};
