#pragma once
#include <iostream>
#include <fstream>
#include "custom_types.h"

using namespace std;
using namespace GraphTypes;

class EdgeListActions
{
public:
    static void readGraph( const string &fileName, edge_list_t &edgList, int &nodeCount, bool &isWeighted, bool &isDirected )
    {
        edgList.clear();
        int edgeCount;
        string line;
        ifstream input ( fileName );
        if ( input.is_open() and getline( input, line ) )
        {
            vector<string> tokens;
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() == 3 )
            {
                nodeCount = stoi( tokens[1] );
                edgeCount = stoi( tokens[2] );
            }
            else 
            {
                cout << "Incorrect input format. Expected 2 values in frist line instead of this: " << line << endl;
                exit(-1);
            }
            
            getline( input, line );
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() == 2 )
            {
                isDirected = stoi( tokens[0] );
                isWeighted = stoi( tokens[1] );
            }
            else 
            {
                cout << "Incorrect input format. Expected 2 values in second line instead of this: " << line << endl;
                exit(-1);
            }
            
            while ( getline( input, line ) )
            {
                GraphTypes::split(tokens, line, ' ');
                
                if( isWeighted ) 
                    edgList.push_back( tuple<int, int, int>( stoi( tokens[0] ), stoi( tokens[1] ), stoi( tokens[2] ) ) );
                else
                    edgList.push_back( tuple<int, int, int>( stoi( tokens[0] ), stoi( tokens[1] ), 0 ) );                
            }
        }
        else cout << "Unable to open file";
    }
    
    static void addEdge( int from, int to, int weight, edge_list_t &edgList )
    {        
        edgList.push_back( make_tuple( from, to, weight ) );
    }    
    
    static void removeEdge( int from, int to, edge_list_t &edgList )
    {
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
            if ( get<0>( *it ) == from && get<1>( *it ) == to )
            {
                edgList.erase( it );
                return;
            }
    }  
    
    static int changeEdge( int from, int to, int newWeight, edge_list_t &edgList )
    {
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
            if ( get<0>( *it ) == from && get<1>( *it ) == to )
            {
                int oldWeight = get<2>( *it );
                edgList.erase( it );
                edgList.push_back( make_tuple( from, to, newWeight ) );
                return oldWeight;
            }
            
        return -1;
    } 
    
    static void transformToAdjMatrix( edge_list_t &edgList, int nodeCount, adjacency_matrix_t &adjMat )
    {
        adjMat.clear();
        for( size_t i = 0; i < nodeCount; i++ )
            adjMat.push_back( adj_mat_line_t( nodeCount, 0 ) );
        
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
            adjMat[ get<0>( *it ) - 1 ][ get<1>( *it ) - 1 ] = get<2>( *it );
    }   
    
    static void transformToAdjList( edge_list_t &edgList, int nodeCount, adjacency_list_t &adjList)
    {
        adjList.clear();
        for( size_t i = 0; i < nodeCount; i++ )
            adjList.push_back( adj_list_line_t() );
            
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
            adjList[ get<0>( *it ) - 1 ].insert( make_tuple( get<0>( *it ), get<1>( *it ), get<2>( *it ) ) ) ;
    }   
    
    static void writeGraph( string fileName, edge_list_t &edgList, int nodeCount, int isWeighted, int isDirected )
    {
        string line;
        ofstream output ( fileName );
        
        output << (char)EDGE_LIST << " " << nodeCount << " " << edgList.size() << endl << isDirected << " " << isWeighted << endl;
        
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        {
            output << to_string( get<0>( *it ) ) << " " <<  to_string( get<1>( *it ) ) << " " << to_string( get<2>( *it ) ) << endl;
        }
    }
    
    static void printGraph( const edge_list_t &edgList, const int nodeCount, const int isWeighted, const int isDirected )
    {
        cout << "Graph type: Edge list" << endl 
            << "Node count: " << nodeCount << endl
            << "Edge count: " << edgList.size() << endl
            << "Is it weighted: " << (isWeighted ? "TRUE" : "FALSE") << endl
            << "Is it directed: " << (isDirected ? "TRUE" : "FALSE") << endl;
        
        string line;
        for ( auto it = edgList.begin(); it != edgList.end(); it++ )
        {
            cout << "Edge (" << to_string( get<0>( *it ) ) << ", " <<  to_string( get<1>( *it ) ) << ") with weight " << to_string( get<2>( *it ) ) << endl;
        }
    }
};
