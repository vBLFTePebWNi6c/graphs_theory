#include <iostream>
#include <fstream>
#include <limits>
#include <algorithm>
#include <stack>
#include <queue>
#include <climits>
#include "custom_types.h"
#include "AdjListActions.h"
#include "EdgeListActions.h"
#include "AdjMatActions.h"
#include "DSU.h"

#define decompose( tuple ) get<0>( tuple ), get<1>( tuple ), get<2>( tuple )
#define reverse_decompose( tuple ) get<1>( tuple ), get<0>( tuple ), get<2>( tuple )

using namespace std;
using namespace GraphTypes;

class Graph
{   
public:
    Graph() : _graph_type( UNKNOWN ), _isWeighted( false ), _isDirected( false ), _nodeCount( 0 ) {}
    
    Graph( int N, graph_representation_t graph_type = EDGE_LIST, bool isWeighted = false, bool isDirected = false ) : _nodeCount( N ), _graph_type( ADJ_MAT ), _isWeighted( isWeighted ), _isDirected( isDirected )
    {
        for( int i = 0; i < N; i++ )
        {
            adj_mat_line_t zeros( N, 0 );
            _adjacency_matrix.push_back( zeros );
        }
        
        switch( graph_type )
        {
            case ADJ_MAT : 
            {
                for( int i = 0; i < N; i++ )
                {
                    adj_mat_line_t zeros( N, 0 );
                    _adjacency_matrix.push_back( zeros );
                } 
                break;
            }
            case EDGE_LIST : this->transformToListOfEdges(); break;
            case ADJ_LIST : this->transformToAdjList(); break;
            default: break;
        }
    }
    
    Graph( string fileName)
    {
        readGraph( fileName );
    }
    
    Graph( const Graph& g ) 
    {
        _graph_type = g._graph_type;
        
        switch( _graph_type )
        {
            case ADJ_MAT : _adjacency_matrix = g._adjacency_matrix; break;
            case EDGE_LIST : _edge_list = g._edge_list; break;
            case ADJ_LIST : _adjacency_list = g._adjacency_list; break;
            default: break;
        }
        
        _isWeighted = g._isWeighted;
        _isDirected = g._isDirected;
        _nodeCount = g._nodeCount;
    }

    void readGraph( string fileName )
    {
        string line;
        ifstream input ( fileName );
        if ( input.is_open() and getline( input, line ) )
        {
            vector<string> tokens;
            GraphTypes::split(tokens, line, ' ');
            if ( tokens.size() > 0 )
                switch ( tokens[0].c_str()[0] )
                {
                    case ADJ_MAT : 
                    {
                        input.close();
                        AdjMatActions::readGraph( fileName, _adjacency_matrix, _nodeCount, _isWeighted, _isDirected );
                        _graph_type = ADJ_MAT; 
                        break;
                    }
                    
                    case ADJ_LIST : 
                    {
                        input.close();
                        AdjListActions::readGraph( fileName, _adjacency_list, _nodeCount, _isWeighted, _isDirected ); 
                        _graph_type = ADJ_LIST;
                        break;
                    }
                    
                    case EDGE_LIST : 
                    {
                        input.close();
                        EdgeListActions::readGraph( fileName, _edge_list, _nodeCount, _isWeighted, _isDirected ); 
                        _graph_type = EDGE_LIST;
                        break;
                    }
                    
                    default : cout << "Wrong representation type: " << line[0] << endl; break;
                }
            else
                cout << "Wrong representation type: " << line << endl;
        }
        else 
        {
            cout << "Unable to open file." << endl;
        } 
    }

    void addEdge( int from, int to, int weight )
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::addEdge( from, to, weight, _adjacency_matrix );
                break;
            }
            
            case ADJ_LIST : 
            {
                AdjListActions::addEdge( from, to, weight, _adjacency_list ); 
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::addEdge( from, to, weight, _edge_list ); 
                break;
            }
            
            default : cout << "Unable add edge to graph of UNKNOWN type." << endl; break; 
        }
    } 
       
    void removeEdge( int from, int to )
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::removeEdge( from, to, _adjacency_matrix );
                break;
            }
            
            case ADJ_LIST : 
            {
                AdjListActions::removeEdge( from, to, _adjacency_list ); 
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::removeEdge( from, to, _edge_list ); 
                break;
            }
            
            default : cout << "Unable remove edge from graph of UNKNOWN type." << endl; break; 
        }
    }
       
    int changeEdge( int from, int to, int newWeight )
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::changeEdge( from, to, newWeight, _adjacency_matrix );
                break;
            }
            
            case ADJ_LIST : 
            {
                AdjListActions::changeEdge( from, to, newWeight, _adjacency_list ); 
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::changeEdge( from, to, newWeight, _edge_list ); 
                break;
            }
            
            default : cout << "Unable add edge to graph of UNKNOWN type." << endl; break; 
        }
    }
    
    void transformToAdjList()
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::transformToAdjList( _adjacency_matrix, _adjacency_list );
                _graph_type = ADJ_LIST;
                break;
            }
            
            case ADJ_LIST : return;
            
            case EDGE_LIST : 
            {
                EdgeListActions::transformToAdjList( _edge_list, _nodeCount, _adjacency_list );
                _graph_type = ADJ_LIST;
                break;
            }
            
            default : cout << "Can not transform UNKNOWN graph type." << endl; break;
        }
    } 
    
    void transformToAdjMatrix()
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : return;
            
            case ADJ_LIST : 
            {
                AdjListActions::transformToAdjMatrix( _adjacency_list, _adjacency_matrix );
                _graph_type = ADJ_MAT;
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::transformToAdjMatrix( _edge_list, _nodeCount, _adjacency_matrix );
                _graph_type = ADJ_MAT;
                break;
            }
            
            default : cout << "Can not transform UNKNOWN graph type." << endl; break;
        }
    }
    
    void transformToListOfEdges()
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::transformToListOfEdges( _adjacency_matrix, _edge_list );
                _graph_type = EDGE_LIST;
                break;
            }
            
            case ADJ_LIST :         
            {
                AdjListActions::transformToListOfEdges( _adjacency_list, _edge_list );
                _graph_type = EDGE_LIST;
                break;
            }
            
            case EDGE_LIST : return;
            
            default : cout << "Can not transform UNKNOWN graph type." << endl; break;
        }
    }
    
    void writeGraph( string fileName )
    {
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::writeGraph( fileName, _adjacency_matrix, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 );
                break;
            }
            
            case ADJ_LIST : 
            {
                AdjListActions::writeGraph( fileName, _adjacency_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::writeGraph( fileName, _edge_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
                break;
            }
            
            default : cout << "Can not write graph of UNKNOWN type." << endl; break;
        }
    }
    
    //lab2
    Graph getSpanningTreePrima() const
    {
        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "Creating copy of graph." << endl;
        Graph g( *this );
        g.transformToAdjList();
        
        set< int > nodes_for_observe;
        nodes_for_observe.insert( 2 );
        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "Creating empty spanning tree." << endl;
        Graph spanningTree( g._nodeCount, EDGE_LIST, g._isWeighted, g._isDirected );
        
        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "Iterating over nodes..." << endl;
        while ( nodes_for_observe.size() != g._nodeCount )
        {
            int min_weight = INT_MAX;
            pair< int, int > edge;
            for ( auto obs_it = nodes_for_observe.begin(); obs_it != nodes_for_observe.end(); obs_it++ )
                for ( auto it = g._adjacency_list[*obs_it - 1].begin(); it != g._adjacency_list[*obs_it - 1].end(); it++ )
                    if ( nodes_for_observe.find( get<1>( *it ) ) != nodes_for_observe.end() )
                    {
                        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "\t Node " << get<1>( *it ) << " already observed." << endl;
                        continue;
                    }
                    else if ( get<2>( *it ) < min_weight )
                    {
                        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "\t Found lesser node " << get<2>( *it ) << endl;
                        min_weight = get<2>( *it );
                        edge = make_pair( get<0>( *it ), get<1>( *it ) );
                    }
                    
            // << "[DEBUG] " << "getSpanningTreePrima(): " << "\t Edge for insert:  " << edge.first << " " << edge.second << endl;
            nodes_for_observe.insert( edge.second );
            spanningTree.addEdge( edge.first, edge.second, min_weight );
            spanningTree.addEdge( edge.second, edge.first, min_weight );
        }
        
        return spanningTree;
    }
    
    Graph getSpanningTreeKruscal() const
    {
        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "Creating copy of graph." << endl;
        Graph g( *this );
        g.transformToListOfEdges();
            
        sort( g._edge_list.begin(), g._edge_list.end(), 
        []( edge_list_line_t first, edge_list_line_t second ) { return get<2>( first ) < get<2>( second ); } );
        //removing duplicates
        for( int i = 0; i < g._edge_list.size(); i++ )
            for( int j = i + 1; j < g._edge_list.size(); j++ )
                if( get<0>( g._edge_list[i] ) == get<1>( g._edge_list[j] ) && get<1>( g._edge_list[i] ) == get<0>( g._edge_list[j] ) ||
                    get<0>( g._edge_list[i] ) == get<0>( g._edge_list[j] ) && get<1>( g._edge_list[i] ) == get<1>( g._edge_list[j] ) )
                {
                    g._edge_list.erase( g._edge_list.begin() + j );
                    j--;
                }
                        
        
        //cout << "[DEBUG] " << "getSpanningTreePrima(): " << "Creating empty spanning tree." << endl;
        DSU dsu( g._nodeCount );
        Graph spanningTree( g._nodeCount, EDGE_LIST, g._isWeighted, g._isDirected );  
              
        for( int i = 0; i < g._edge_list.size(); i++ )  
            if( dsu.unite( get<0>( g._edge_list[i] ) - 1, get<1>( g._edge_list[i] ) - 1 ) )
                spanningTree.addEdge( decompose( g._edge_list[i] ) );
        
        return spanningTree;
    }
    
    Graph getSpanningTreeBoruvka() const
    {
        Graph g( *this );
        g.transformToAdjList();
        
        Graph spanningTree( g._nodeCount, EDGE_LIST, g._isWeighted, g._isDirected );
        DSU_plus dsu( g._nodeCount );
        
        while( dsu.components.size() != 1 )
        {            
            vector< edge_list_line_t > edges;
            for( int i = 0; i < dsu.components.size(); i++ )
            {
                edge_list_line_t edge = make_tuple( -1, -1, INT_MAX);
                for( int j = 0; j < dsu.components[i].size(); j++ )
                {
                    for ( auto it = g._adjacency_list[dsu.components[i][j]].begin(); it != g._adjacency_list[dsu.components[i][j]].end(); it++ )
                        if ( find( dsu.components[i].begin(), dsu.components[i].end(), get<1>( *it ) - 1 ) != dsu.components[i].end() )
                            continue;
                        else if ( get<2>( *it ) < get<2>( edge ) )
                            edge = *it;
                            
                }
                edges.push_back( edge );
            }
            
            for( int i = 0; i < edges.size(); i++ )
            {
                dsu.unite( get<0>( edges[i] ) - 1, get<1>( edges[i] ) - 1 );                
                spanningTree.addEdge( decompose( edges[i] ) );
                spanningTree.addEdge( reverse_decompose( edges[i] ) );
            }  
            edges.clear();          
        }
        
        return spanningTree;
    }
    
    //lab3
    
    int checkEuler( bool &circleExist ) const
    {
        Graph g( *this );
        if ( g._graph_type != EDGE_LIST )
            g.transformToListOfEdges();
            
        DSU_plus dsu( g._nodeCount );
        vector< int > nodesDegree( g._nodeCount, 0 );
            
        for ( auto it = g._edge_list.begin(); it != g._edge_list.end(); it++ )
        {
            dsu.unite( get<0>( *it ) - 1, get<1>( *it ) - 1 );
            nodesDegree[get<0>( *it ) - 1]++;
            nodesDegree[get<1>( *it ) - 1]++;
        }
            
        if ( dsu.components.size() != 1 )
        {
            //cout << "Components count: " << dsu.components.size() << endl;
            //for(  int i = 0; i < dsu.components.size(); i++ )
            //{
            //    cout << i + 1 << " c.: ";
            //    for( int j = 0; j < dsu.components[i].size(); j++ )
            //        cout << dsu.components[i][j] + 1 << ' ';
            //    cout << endl;
            //}
                
            circleExist = false;
            return 0;
        }
        
        int nonParityCount = 0;
        int parityNode = -1, nonParityNode = -1;
        for( int i = 0; i < nodesDegree.size(); i++ )
            if ( nodesDegree[i] % 2 != 0 )
            {
                nonParityCount++;
                nonParityNode = i + 1;
            }
            else
                parityNode = i + 1;
                
        //cout << "Non parity count: " << nonParityCount << endl;
        circleExist = nonParityCount == 0;
        
        if( circleExist || nonParityCount == 2 )
            return circleExist ? parityNode : nonParityNode;
        else return 0;
    }
    
    vector< int > getEuleranTourFleri() const
    {
        vector< int > result;
        bool hasCircle;
        int startNode = checkEuler( hasCircle );
        if ( startNode == 0 )
        {
            cout << "Graph hasn\'t euler circle or path." << endl;
            return result;
        }
        else
            result.push_back( startNode );
        
        Graph g( *this );
        if ( g._graph_type != ADJ_LIST )
            g.transformToAdjList();
            
        int nextNode = startNode - 1;
            
        while( result.size() != this->_nodeCount )
        {
            bool stepHappened = false;
            for ( auto it = g._adjacency_list[nextNode].begin(); it != g._adjacency_list[nextNode].end(); it++ )
                if( !checkBridge( g, get<0>( *it ) , get<1>( *it ) ) )
                {
                    g.removeEdge( get<0>( *it ) , get<1>( *it ) );
                    nextNode = get<1>( *it ) - 1;
                    result.push_back( nextNode + 1 );
                    stepHappened = true;
                    break;
                }
                
            if ( !stepHappened )
            {
                auto it = g._adjacency_list[nextNode].begin();
                g.removeEdge( get<0>( *it ) , get<1>( *it ) );
                nextNode = get<1>( *it ) - 1;
                result.push_back( nextNode + 1 );
            }
        }
        
        return result;
    }
    
    vector< int > getEuleranTourEffective() const
    {
        vector< int > result;
        bool hasCircle;
        int startNode = checkEuler( hasCircle );
        if ( startNode == 0 )
        {
            cout << "Graph hasn\'t euler circle or path." << endl;
            return result;
        }
        
        Graph g( *this );
        if ( g._graph_type != EDGE_LIST )
            g.transformToListOfEdges();
           
        stack< int > S;
        S.push( startNode );
        while ( S.size() != 0 )
        {
            int w = S.top();
            for ( auto it = g._edge_list.begin(); it != g._edge_list.end(); it++ )
                if ( w == get<0>( *it ) ) 
                {
                    S.push( get<1>( *it ) );
                    g.removeEdge( w, get<1>( *it ) );
                    //g.printGraph();
                    break;
                }
            
            if ( w == S.top() )
            {
                //cout << w + 1 << endl;
                S.pop();
                result.push_back( w );
            }
        }
        
        reverse( result.begin(), result.end() );
        result.pop_back();
        return result;
    }
    
    //lab4
    int checkBipart( vector< char > &marks ) const
    {
        struct Checker
        {
            Checker( int nodeCount, int firstNode = 1, tuple< char, char, char > customNames = make_tuple( 'A', 'B', 'Z' ) )
            {
                marks = vector< char >( nodeCount, get<2>( customNames ) );
                marksNames = make_pair( get<0>( customNames ), get<1>( customNames ) );
                marks[firstNode - 1] = marksNames.first;
            }
            
            bool check( int w, int u, queue< int > &Q )
            {
                w = w - 1;
                u = u - 1;
                
                bool uIsNotMarked = ( marks[u] != marksNames.first && marks[u] != marksNames.second );
                
                if ( marks[w] == marksNames.first )
                {
                    if( uIsNotMarked )
                    {
                        marks[u] = marksNames.second;
                        Q.push( u + 1 );
                        return true;
                    }
                    else if ( marks[u] == marksNames.first )
                        return false;
                }
                else if ( marks[w] == marksNames.second )
                {
                    if( uIsNotMarked )
                    {
                        marks[u] = marksNames.first;
                        Q.push( u + 1 );
                        return true;
                    }
                    else if ( marks[u] == marksNames.second )
                        return false;
                }
                else
                    cout << "Node " << w + 1 << " wasn\'t marked before." << endl;
                    
                return true;
            }
            
            vector< char > marks;
            pair< char, char > marksNames; 
        };
        
        int firstNode = 1;
        Checker c( this->_nodeCount, firstNode );
        queue< int > Q;
        Q.push( 1 );
        
        Graph g( *this );
        if ( g._graph_type != EDGE_LIST )
            g.transformToListOfEdges();
            
        //cout << g._edge_list.size() << " - " << endl;
            
        //removing duplicates
        for( int i = 0; i < g._edge_list.size(); i++ )
            for( int j = i + 1; j < g._edge_list.size(); j++ )
                if( get<0>( g._edge_list[i] ) == get<1>( g._edge_list[j] ) && get<1>( g._edge_list[i] ) == get<0>( g._edge_list[j] ) ||
                    get<0>( g._edge_list[i] ) == get<0>( g._edge_list[j] ) && get<1>( g._edge_list[i] ) == get<1>( g._edge_list[j] ) )
                {
                    g._edge_list.erase( g._edge_list.begin() + j );
                    j--;
                }
        
        //cout << g._edge_list.size() << endl;
        //g.printGraph();
            
        while ( Q.size() != 0 )
        {
            int w = Q.front();
            //cout << "Select node " << w << endl;
            Q.pop();
            for ( auto it = g._edge_list.begin(); it != g._edge_list.end(); it++ )
                if ( w == get<0>( *it ) && !c.check( w, get<1>( *it ), Q ) ) 
                {
                    marks = c.marks;
                    return (int)false;
                }
                else if ( w == get<1>( *it ) && !c.check( w, get<0>( *it ), Q ) ) 
                {
                    marks = c.marks;
                    return (int)false;
                }
                
            //queue< int > printable( Q );
            //while( printable.size() != 0 )
            //{
                //cout << printable.front() << " ";
                //printable.pop();
            //}
            
        }
        
        marks = c.marks;
        return (int)true;
        
    }
    
    vector< pair< int, int > > getMaximumMatchingBipart() const 
    {
        vector< pair< int, int > > result;
        
        vector< char > marks;
        if ( !checkBipart( marks ) )
        {
            cout << "Graph is not bipart." << endl;
            return result;
        }
        
        pair< vector< int >, vector< int > > masks;
        vector< int > reverse_mask( marks.size() );
        int n = 0, k = 0;
        for( int i = 0; i < marks.size(); i++ )
            if( marks[i] == marks[0] )
            {
                n++;
                masks.first.push_back( i );
                reverse_mask[i] = n - 1;
            }
            else
            {
                k++;
                masks.second.push_back( i );
                reverse_mask[i] = k - 1;
            }
                
        vector < vector<int> > g;
        Graph g_copy( *this );
        g_copy.transformToAdjList();
        for( int i = 0; i < masks.first.size(); i++ )
        {
            vector< int > nodes;
            for ( auto it = g_copy._adjacency_list[masks.first[i]].begin(); it != g_copy._adjacency_list[masks.first[i]].end(); it++ )
                nodes.push_back( reverse_mask[ get<1>( *it ) - 1 ] );
            g.push_back( nodes );
        }
        
        vector<int> mt( k, -1 );
        vector<bool> used( n, false );
        
        struct i_just_need_recursive_function_inside
        {
            static bool try_kuhn (int v, 
                                  vector<bool> &used, 
                                  vector<int> &mt, 
                                  vector < vector<int> > &g,
                                  int n, int k ) 
            {
                if (used[v])  return false;
                used[v] = true;
                for (size_t i=0; i<g[v].size(); ++i) {
                    int to = g[v][i];
                    if (mt[to] == -1 || try_kuhn (mt[to], used, mt, g, n, k )) {
                        mt[to] = v;
                        return true;
                    }
                }
                return false;
            }
        };
        
        
        for (int v=0; v<n; ++v) {
            used.assign (n, false);
            i_just_need_recursive_function_inside::try_kuhn( v, used, mt, g, n, k );
        }
    
        for (int i=0; i<k; ++i)
            if (mt[i] != -1)
                result.push_back( make_pair( masks.first[mt[i]] + 1, masks.second[i] + 1 ) );
    }
    
    //lab5
    Graph flowFordFulkerson()
    {
        
    }
    
    Graph flowDinitz()
    {
        
    }
    
    //stuff
    void printGraph() const
    {
        bool eulerCircleExist, eulerPathExist;
        int startNode = checkEuler( eulerCircleExist );
        eulerPathExist = startNode != 0 && !eulerCircleExist;
        
        vector< char > marks;
        bool graphIsBipart = this->checkBipart( marks );
        
        switch ( _graph_type )
        {
            case ADJ_MAT : 
            {
                AdjMatActions::printGraph( _adjacency_matrix, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 );
                break;
            }
            
            case ADJ_LIST : 
            {
                AdjListActions::printGraph( _adjacency_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
                break;
            }
            
            case EDGE_LIST : 
            {
                EdgeListActions::printGraph( _edge_list, _nodeCount, _isWeighted ? 1 : 0, _isDirected ? 1 : 0 ); 
                break;
            }
            
            default : cout << "Can not print graph of UNKNOWN type." << endl; break;
        }
        
        if ( eulerCircleExist )
            cout << "Graph has Euler circle." << endl << "Start node: " << startNode << endl;
        else if ( eulerPathExist )
            cout << "Graph has Euler path." << endl << "Start node: " << startNode << endl;
        else
            cout << "Graph has not Euler path." << endl;
            
        if ( graphIsBipart )
        {
            cout << "Graph is bipart." << endl << "Marks: ";
            for( auto it = marks.begin(); it != marks.end(); it++ )
                cout << (char)*it << " ";
            cout << endl;
        }
        else
            cout << "Graph is not bipart." << endl;
    }
    
private:      
    adjacency_matrix_t _adjacency_matrix;
    adjacency_list_t _adjacency_list;
    edge_list_t _edge_list;
    
    graph_representation_t _graph_type;
    bool _isWeighted;
    bool _isDirected;
    int _nodeCount;
    
    //stuff
    bool checkBridge( Graph g, int from, int to ) const
    {             
        //cout << "Removing edge " << from << " " << to << endl;
        g.removeEdge( from, to );
        
        DSU_plus dsu( g._nodeCount );
        
        //cout << "Uniting components." << endl;
        switch( g._graph_type )
        {
            case ADJ_MAT :
                for( int i = 0; i < g._nodeCount; i++ )
                    for( int j = 0; j < g._nodeCount; j++ )
                        if( g._adjacency_matrix[i][j] != 0 )
                            dsu.unite( i, j );
            break;
            case EDGE_LIST : 
                for ( auto it = g._edge_list.begin(); it != g._edge_list.end(); it++ )
                    dsu.unite( get<0>( *it ) - 1, get<1>( *it ) - 1 );
            break;
            case ADJ_LIST : 
                for( int i = 0; i < g._nodeCount; i++ )
                    for ( auto it = g._adjacency_list[i].begin(); it != g._adjacency_list[i].end(); it++ )
                        dsu.unite( get<0>( *it ) - 1, get<1>( *it ) - 1 );
            break;
            default: break;
        }    
            
        if ( dsu.components.size() != 1 )
            return false;
        else 
            return true;        
    }
};

