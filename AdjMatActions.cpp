#include "AdjMatActions.h"

void AdjMatActions::readGraph( const string &fileName, adjacency_matrix_t &adjMat, int &nodeCount, bool &isWeighted, bool &isDirected )
{
    string line;
    ifstream input ( fileName );
    if ( input.is_open() and getline( input, line ) )
    {
        vector<string> tokens;
        boost::split(tokens, line, boost::is_any_of(" "));
        if ( tokens.size() == 4 )
        {
            nodeCount = stoi( tokens[1] );
            isWeighted = stoi( tokens[2] );
            isDirected = stoi( tokens[3] );
        }
        else 
        {
            cout << "Incorrect input format. Expected 4 values in frist line instead of this: " << line << endl;
            exit(-1);
        }
        
        while ( getline( input, line ) )
        {
            boost::split(tokens, line, boost::is_any_of(" "));
            adj_mat_line_t adj_mat_line;
            for ( size_t i = 0; i < tokens.size(); i++ )
                adj_mat_line.push_back( stoi( tokens[i] ) );
                
            adjMat.push_back( adj_mat_line );
        }
    }
    else cout << "Unable to open file"; 
}

void AdjMatActions::addEdge( int from, int to, int weight, adjacency_matrix_t &adjMat )
{
    adjMat[from - 1][to - 1] = weight;
}

void AdjMatActions::removeEdge( int from, int to, adjacency_matrix_t &adjMat )
{
    adjMat[from - 1][to - 1] = 0;
}

int AdjMatActions::changeEdge( int from, int to, int newWeight, adjacency_matrix_t &adjMat )
{
    adjMat[from - 1][to - 1] = newWeight;
}

void AdjMatActions::transformToAdjList( adjacency_matrix_t &adjMat, adjacency_list_t &adjList)
{
    adjList.clear();
    adj_list_line_t adj_list_line;
    for ( size_t i = 0; i < adjMat.size(); i++ )
    {
        for ( size_t j = 0; j < adjMat.size(); j++ )
            if ( adjMat[i][j] != 0 ) 
                adj_list_line.insert( tuple<int, int, int>( i + 1, j + 1, adjMat[i][j] ) );
        adjList.push_back( adj_list_line );
    }
}

void AdjMatActions::transformToListOfEdges( adjacency_matrix_t &adjMat, edge_list_t &edgList )
{
    edgList.clear();
    for ( size_t i = 0; i < adjMat.size(); i++ )
    {
        for ( size_t j = 0; j < adjMat.size(); j++ )
            if ( adjMat[i][j] != 0 ) 
            {
                edgList.insert( tuple<int, int, int>( i + 1, j + 1, adjMat[i][j] ) );
            }
    }
}

void AdjMatActions::writeGraph( string fileName, adjacency_matrix_t &adjMat, int nodeCount, int isWeighted, int isDirected )
{
    string line;
    ofstream output ( fileName );
    
    output << (char)ADJ_MAT << " " << nodeCount << " " << isWeighted << " " << isDirected << endl;
    
    for ( size_t i = 0; i < adjMat.size(); i++ )
    {
        line = "";
        for ( size_t j = 0; j < adjMat.size(); j++ )
        {
            line += to_string( adjMat[i][j] ) + " ";
        }
        output << line << endl;
    }
}

void AdjMatActions::printGraph( adjacency_matrix_t &adjMat, int nodeCount, int isWeighted, int isDirected )
{
    cout << "Graph type: Adjacency matrix" << endl 
         << "Node count: " << nodeCount << endl
         << "Is it weighted: " << (isWeighted ? "TRUE" : "FALSE") << endl
         << "Is it directed: " << (isDirected ? "TRUE" : "FALSE") << endl;
    
    string line;
    for ( size_t i = 0; i < adjMat.size(); i++ )
    {
        line = "";
        for ( size_t j = 0; j < adjMat.size(); j++ )
        {
            line += to_string( adjMat[i][j] ) + " ";
        }
        cout << line << endl;
    }
}
